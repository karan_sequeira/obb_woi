#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "platform/CCPlatformConfig.h"
#include "jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#include "android/asset_manager.h"
#include "android/asset_manager_jni.h"
#include "jni/CocosPlayClient.h"
#include "base/ZipUtils.h"
#include "WOIFileUtilsAndroid.h"

/*
 * If the ENABLE_ENCRYPTION flag is enabled in Application.mk,
 * the string specified by KEY will be used to read the password protected file.
 * karan.sequeira@w-o-i.com on 01/07/2015
 * */
#ifdef ENABLE_ENCRYPTION
#define KEY "woigames"
#endif

WOIFileUtilsAndroid::WOIFileUtilsAndroid()
{
	language_id_ = std::string("EN");
}

WOIFileUtilsAndroid::~WOIFileUtilsAndroid()
{
	CC_SAFE_DELETE(obb_file_);
}

bool WOIFileUtilsAndroid::init()
{
	std::string main_obb_path = std::string(getApkPath());
	if(main_obb_path.find("obb") != std::string::npos)
	{
		obb_file_ = new ZipFile(main_obb_path, "assets/");
	}
	return FileUtilsAndroid::init();
}

void WOIFileUtilsAndroid::setLanguageID()
{
	std::string language_code = std::string(Application::getInstance()->getCurrentLanguageCode());
	std::transform(language_code.begin(), language_code.end(), language_code.begin(), ::toupper);

	char path_to_enable_language[512] = {0};
	sprintf(path_to_enable_language, "Localization/%s/Language.cfg", language_code.c_str());

	auto enable_language = isFileExist(fullPathForFilename(path_to_enable_language).c_str());
	if(enable_language)
	{
		language_id_[0] = language_code[0];
		language_id_[1] = language_code[1];
	}
}

std::string WOIFileUtilsAndroid::getStringFromFile(const std::string& filename)
{
    Data data = getDataFromFile(filename);
    if (data.isNull())
        return "";

    ssize_t size = data.getSize();
    unsigned char* data_char = data.getBytes();
    data_char[size] = '\0';

    std::string ret((const char*)data_char);
    return ret;
}

Data WOIFileUtilsAndroid::getDataFromFile(const std::string& filename)
{
	std::string modified_filename = std::string(filename);
	std::size_t found_en = modified_filename.find("/EN/");
	if(found_en != std::string::npos)
	{
		modified_filename[found_en + 1] = language_id_[0];
		modified_filename[found_en + 2] = language_id_[1];
	}

	unsigned char *data = nullptr;
	ssize_t size = 0;

	std::string full_path = fullPathForFilename(modified_filename);
	if(full_path[0] != '/')
	{
		/*
		 * Added support to read from a password protected archive
		 * karan.sequeira@w-o-i.com on 01/07/2015
		 * */
		std::string password = "";
#ifdef ENABLE_ENCRYPTION
		password = std::string(KEY);
#endif

		if(obb_file_ && ((data = obb_file_->getFileData(full_path, &size, password)) != nullptr))
		{
			Data ret;
			ret.fastSet(data, size);
			return ret;
		}
	}

	return FileUtilsAndroid::getDataFromFile(modified_filename);
}

bool WOIFileUtilsAndroid::isFileExistInternal(const std::string& strFilePath) const
{
	if (strFilePath.empty())
	{
		return false;
	}

	if (cocosplay::isEnabled() && !cocosplay::isDemo())
	{
		return cocosplay::fileExists(strFilePath);
	}

	bool bFound = false;

	if (strFilePath[0] != '/')
	{
		const char* s = strFilePath.c_str();

		// Found "assets/" at the beginning of the path and we don't want it
		if (strFilePath.find(_defaultResRootPath) == 0) s += strlen("assets/");

		// check if file exists in OBB
		if(obb_file_ && obb_file_->fileExists(strFilePath))
		{
			bFound = true;
		}
		else if (FileUtilsAndroid::getAssetManager())
		{
			AAsset* aa = AAssetManager_open(FileUtilsAndroid::getAssetManager(), s, AASSET_MODE_UNKNOWN);
			if (aa)
			{
				bFound = true;
				AAsset_close(aa);
			}
			else
			{
				// CCLOG("[AssetManager] ... in APK %s, found = false!", strFilePath.c_str());
			}
		}
	}
	else
	{
		FILE *fp = fopen(strFilePath.c_str(), "r");
		if(fp)
		{
			bFound = true;
			fclose(fp);
		}
	}
	return bFound;
}

#endif
