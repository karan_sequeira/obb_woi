#ifndef WOI_FILE_UTILS_ANDROID_H_
#define WOI_FILE_UTILS_ANDROID_H_

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "cocos2d.h"
#include "platform/CCPlatformConfig.h"
#include "platform/android/CCFileUtils-android.h"

USING_NS_CC;

class ZipUtils;

class WOIFileUtilsAndroid : public FileUtilsAndroid
{
	friend class FileUtilsAndroid;
public:
	WOIFileUtilsAndroid();
	virtual ~WOIFileUtilsAndroid();

	bool init();

	/**
	 *  Gets string from a file.
	 */
	virtual std::string getStringFromFile(const std::string& filename) override;

	/**
	 *  Creates binary data from a file.
	 *  @return A data object.
	 */
	virtual cocos2d::Data getDataFromFile(const std::string& filename) override;

	/*
	 * Sets the two-letter-id for the currently selected system language.
	 * */
	void setLanguageID();

	/*
	 * Returns the two-letter-id for the currently selected system language.
	 * */
	std::string getLanguageID() { return language_id_; }

protected:
	/**
	 *  Checks whether a file exists without considering search paths and resolution orders.
	 *  @param filename The file (with absolute path) to look up for
	 *  @return Returns true if the file found at the given absolute path, otherwise returns false
	 */
	virtual bool isFileExistInternal(const std::string& filename) const;

	std::string language_id_;

private:
	ZipFile* obb_file_ = nullptr;
};

#endif

#endif // WOI_FILE_UTILS_ANDROID_H_
