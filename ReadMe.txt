OBB Reading Module ReadMe
-------------------------

To add OBB reading support:
---------------------------
1. Refer to the following files and modify the ones located under the 'cocos/platform/android/java/src/org/cocos2dx/lib/' folder of your engine:
   |- Cocos2dxHelper.java
   |- Cocos2dxMusic.java
   `- Cocos2dxSound.java

   The comments in the above files describe the code that needs to be changed/added.
   It would help to compare code between the files uploaded to you and the default ones from the engine.

2. Include the WOIFileUtilsAndroid.h & WOIFileUtilsAndroid.cpp files in your game's C++ source.

3. Initiate the WOIFileUtilsAndroid class in your game's AppDelegate::applicationDidFinishLaunching function as follows:
	#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	    auto woi_file_utils_android = new WOIFileUtilsAndroid();
	    if(woi_file_utils_android->init())
	    {
	    	FileUtils::getInstance()->setDelegate(woi_file_utils_android);
	    }
	    else
	    {
	    	delete woi_file_utils_android;
	    	woi_file_utils_android = nullptr;
	    }
	#endif
